package local.rss.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import local.rss.object.*;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pankaj Bhambhani
 */


public class XMLParser {

    private DocumentBuilder builder;
    private XPath path;

    public XMLParser() throws ParserConfigurationException {

        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        //System.out.println(dbfactory.isNamespaceAware());
        dbfactory.setNamespaceAware(true);
        builder = dbfactory.newDocumentBuilder();
        XPathFactory xpfactory = XPathFactory.newInstance();
        path = xpfactory.newXPath();

        NamespaceContext context = new NamespaceContextMap (
                "atom", "http://www.w3.org/2005/Atom",
                "openSearch", "http://a9.com/-/spec/opensearch/1.1/",
                "georss", "http://www.georss.org/georss",
                "thr", "http://purl.org/syndication/thread/1.0",
                "media", "http://search.yahoo.com/mrss/",
                "feedburner", "http://rssnamespace.org/feedburner/ext/1.0");
      path.setNamespaceContext(context);
    }

    public Rss parse(String filename) throws SAXException, IOException, XPathExpressionException {

        File f = new File(filename);
        Document doc = builder.parse(f);

        Rss rss = new Rss();

        int channelCount = Integer.parseInt(path.evaluate("count(/rss/channel)", doc));

        ArrayList<Channel> channels = new ArrayList<Channel>();

           for (int i = 1; i <=channelCount; i++)  {

               Channel channel = new Channel();
               String cpath = "/rss/channel[" + i + "]";
               String title = path.evaluate(cpath + "/title", doc);
               String description = path.evaluate(cpath + "/description", doc);
               String link = path.evaluate(cpath + "/link", doc);
               String managingEditor = path.evaluate(cpath + "/managingEditor", doc);
               int itemCount = Integer.parseInt(path.evaluate("count(" + cpath + "/item)", doc));

               ArrayList<Item> items = new ArrayList<Item>();

               for (int j = 1; j <= itemCount; j ++) {

                   Item item = new Item();

                   String ipath = cpath + "/item[" + j + "]";
                   String puDate = path.evaluate(ipath + "/pubDate", doc);
                   String updated = path.evaluate(ipath + "/atom:updated", doc);
                   String category = path.evaluate(ipath + "/category", doc);
                   String itemTitle = path.evaluate(ipath + "/title", doc);
                   String itemDescription = path.evaluate(ipath + "/description", doc);
                   String itemLink = path.evaluate(ipath + "/link", doc);
                   String author = path.evaluate(ipath + "/author", doc);
                  // XPathExpression xpression = path.compile(ipath + "/feedburner:origLink");
                   String origLink = path.evaluate(ipath+"/feedburner:origLink", doc);

                   item.setPubDate(puDate);
                   item.setUpdated(updated);
                   item.setCategory(category);
                   item.setTitle(itemTitle);
                   item.setDescription(itemDescription);
                   item.setLink(itemLink);
                   item.setAuthor(author);
                   item.setOrigLink(origLink);

                   items.add(item);
               }

               channel.setTitle(title);
               channel.setDescription(description);
               channel.setLink(link);
               channel.setManagingEditor(managingEditor);
               channel.setItems(items);

               channels.add(channel);
           }

        rss.setChannels(channels);
        return rss;
    }
}
