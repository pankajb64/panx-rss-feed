/*
* @author Pankaj Bhambhani
 */

package local.rss.net;

import java.io.*;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 *
 * @author Administrator
 */
public class URLGet {

    private String urlString;
    private String feed = "";
    
    public URLGet(String urlString)  {

        this.urlString = urlString;
    }

    public boolean fetch() throws IOException {

        try {


        //Open Connection
        URL url = new URL (urlString);

        URLConnection connection = url.openConnection();

        //Check if response code is HTTP_OK (200)
        HttpURLConnection httpconnection = (HttpURLConnection) connection;

        int code = httpconnection.getResponseCode();
        String message = httpconnection.getResponseMessage();

        System.out.println(code + " " + message);

        if(code != HttpURLConnection.HTTP_OK )
            return false;

        //Read server response
        InputStream instream = connection.getInputStream();
        Scanner in = new Scanner(instream);
        
        while(in.hasNextLine()) {

            String line = in.nextLine();
            feed += line;
        }

        return true;
        }catch(Exception e) {
            return false;
        }
    }

    public void clear () {

        feed = "";
    }

    public void save (String filename) throws IOException {

        
        PrintWriter printer = new PrintWriter (filename);

        printer.println(feed);

        printer.flush();
        printer.close();

    }
}
