/*
 * @author Pankaj Bhambhani
 */

package local.rss.object;

import local.rss.util.Utilities;
/**
 *
 * @author Administrator
 */
public class Item {

    private String pubDate;
    private String  updated;
    private String  category;
    private String title;
    private String  description;
    private String   link;
    private String   author;
    private String   origLink;

    public String getPubDate () {

        return pubDate;
    }

    public String getUpdated () {

        return updated;
    }
    public String getCategory () {

        return category;
    }
    public String getTitle () {

        return title;
    }
    public String getDescription () {

        return description;
    }
    public String getLink () {

        return link;
    }
    public String getAuthor () {

        return author;
    }
    public String getOrigLink () {

        return origLink;
    }

    public void setPubDate (String pubDate) {

        this.pubDate = pubDate;
    }

    public void setUpdated (String updated) {

        this.updated =  updated;
    }
    public void setCategory (String category) {

       this.category =  category;
    }
    public void setTitle (String title) {

        this.title = title;
    }
    public void setDescription (String description) {

        this.description = description;
    }
    public void setLink (String link) {

        this.link = link;
    }
    public void setAuthor (String author) {

        this.author = author;
    }
    public void setOrigLink (String origLink) {

        this.origLink =  origLink;
    }

    @Override
    public String toString () {
        
        return "<h4>Item</h4><br/>Published Date - "
                + pubDate
                + "<br/>Updated - "
                + updated
                + "<br/>Category - "
                + category
                + "<br/><b>Title - "
                + title
                + "</b><br/><p>"
                + description
                + "</p><br/>Author - "
                + author
                + "<br/> Original Link - "
                + Utilities.toLink(origLink)
                + "<br/>";
    }

    public String format () {

        return "<div class=\"Item\">Item<br/>"
                + "<h1 class=\"hider\">Title - "
                + title
                + "(Click to Reveal or Hide)</h1><br/>"
                + "<div class=\"hidden\"><p>"
                + description
                + "</p><br/>Author - "
                + author
                + "<br/>Published Date - "
                + pubDate
                + "<br/>Updated - "
                + updated
                + "<br/>Category - "
                + category
                + "<br/> Original Link - "
                + Utilities.toLink(origLink)
                + "</div></div><br/>";
    }
}
