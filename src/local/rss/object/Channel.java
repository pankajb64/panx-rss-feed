/*
 * @author Pankaj Bhambhani
 */

package local.rss.object;

import java.util.ArrayList;
import local.rss.util.Utilities;

/**
 *
 * @author Administrator
 */
public class Channel {

    private String title;
    private String description;
    private String link;
    private String managingEditor;
    private ArrayList<Item> items;

    public String getTitle() {

        return title;
    }

    public String getDescription() {

        return description;
    }

    public String getLink() {

        return link;
    }

    public String getManagingEditor() {

        return managingEditor;
    }

    public ArrayList<Item> getItems() {

        return items;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public void setLink(String link) {

        this.link = link;
    }

    public void setManagingEditor(String managingEditor) {

        this.managingEditor = managingEditor;
    }

    public void setItems(ArrayList<Item> items) {

        this.items = items;
    }

    @Override
    public String toString() {

        return "<h1>Title - " 
                + title
                + "</h1><br/><h2>"
                + description
                + "</h2><br/><h3>"
                + Utilities.toLink(link)
                + "<br />"
                + managingEditor
                + "</h3><br/>"
                + getItemsAsString()
                + "<br/>";
    }

    private String getItemsAsString() {

        String str = "";

        for (Item item : items) {
           
            str += item.format() + "\n";
        }
        return str;
            
     }

    public String format () {

        return "<div class=\"Channel\">Channel<br/>"
                + "<h1 class=\"hider\">Title - "
                + title
                + " (Click to Reveal or Hide)</h1>"
                + "<div class=\"hidden\"><h4>"
                + description
                + "</h4><h4>"
                + Utilities.toLink(link)
                + "<br/>Managing Editor - "
                + managingEditor
                + "</h4>"
                + getItemsAsString()
                + "<br/></div></div>";
    }

    
}
