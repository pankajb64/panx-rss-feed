/*
 * @author Pankaj Bhambhani
 */

package local.rss.object;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Rss {

    private ArrayList<Channel> channels;

    public ArrayList<Channel> getChannels() {

        return channels;
    }

    public void setChannels(ArrayList<Channel> channels) {

        this.channels = channels;
    }

    @Override
    public String toString() {

        return "RSS Feed - <br />"
                + getChannelsAsString()
                + "<br/>" ;
    }

    private String getChannelsAsString() {

        String str = "";
        
        for (Channel channel : channels)
                    str += channel.format() + "\n";

        return str;

    }

    public String format() {

        return "<div class=\"Rss\""
                + getChannelsAsString()
                + "<br/>" ;
    }

    public static Rss join(ArrayList<Rss> feeds) {

        Rss rss = new Rss ();
        ArrayList<Channel> channels = new ArrayList<Channel>();

        for(Rss r: feeds) {

            for(Channel ch : r.getChannels())
                        channels.add(ch);
            
        }

        rss.setChannels(channels);
        return rss;
    }
}
