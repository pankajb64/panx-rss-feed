/*
 * @author Pankaj Bhambhani
 */

package local.rss.test;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import local.rss.parser.XMLParser;
import local.rss.object.*;
/**
 *
 * @author Administrator
 */
public class XMLParserTester {

    public static void main(String[] args) throws Exception {

        XMLParser parser = new XMLParser();

        String filename = "C:\\test\\feed.xml";
        Rss rss = parser.parse(filename);

        BufferedReader reader = new BufferedReader(new FileReader("C:\\test\\cover.txt"));

        String html = "";
        while(true) {

            String line = reader.readLine();

            if(line == null)
                break;

            html += line;
        }

        PrintWriter printer = new PrintWriter("C:\\test\\test(3).html");

        //printer.println("<html>\n</head>\n<title>RSS Feed</title>\n<body >");
        printer.println(html);
        printer.println(rss.toString());
        printer.println("\n</body>\n</html>");
        printer.flush();

        printer.close();
        
    }
}
