/*
 * @author Pankaj Bhambhani
 */

package local.rss.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import local.rss.net.URLGet;
import local.rss.object.Rss;
import local.rss.parser.XMLParser;

/**
 *
 * @author Administrator
 */
public class AggregateTester {

    public static void main (String[] args) throws Exception {

        
    ArrayList<String> feedURLs = getFeedURLs("feedlist.txt");
    ArrayList<Rss> feeds = getFeeds (feedURLs);
   
        String html = getCover("cover.html");
        Rss rss = Rss.join(feeds);
        PrintWriter printer = new PrintWriter("test.html");

//        printer.println("<html>\n</head>\n<title>RSS Feed</title>\n<body style=\"text-align:center\">");
        printer.println(html);
        printer.println(rss.toString());
        printer.println("\n</body>\n</html>");
        printer.flush();

        printer.close();
        
 }

    private static String getCover(String filename) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(filename));

        String html = "<!--Generated html file - by Pankaj Bhambhani-->\n";
        String line = reader.readLine(); // ignore first line
        while(true) {

            line = reader.readLine();

            if(line == null)
                break;

            html += line+"\n";
        }
        return html;
    }

    private static ArrayList<String> getFeedURLs(String filename) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(filename));

        ArrayList<String> list = new ArrayList<String>();

        while (true) {

            String line = reader.readLine();

            if(line == null)
                break;
            list.add(line);
        }
        return list;

    }

    private static ArrayList<Rss> getFeeds (ArrayList<String> feedURLs)  throws Exception{

        ArrayList<Rss> feeds = new ArrayList<Rss>();
        int count = 0;
        for(String urlString : feedURLs) {


            URLGet uget = new URLGet (urlString);
            boolean result = uget.fetch();

            String savefilename = "feed" + count++ + ".xml";

            if(result)
             uget.save(savefilename);

            XMLParser parser = new XMLParser();

            Rss rss = parser.parse(savefilename);

            feeds.add(rss);
        }

        return feeds;
    }
}
